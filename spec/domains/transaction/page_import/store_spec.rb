require 'rails_helper'

describe Transaction::PageImport::Store do
  subject { described_class }

  context '#call' do
    let(:transaction_object) do
      Transaction::PageImport::TransactionObject.new.tap do |object|
        object.date_of_payment = Date.today
        object.amount = 1
      end
    end

    it 'creates user transaction' do
      expect {
        subject.new([transaction_object]).call
      }.to change { UserTransaction.count  }.by(1)
    end
  end
end
