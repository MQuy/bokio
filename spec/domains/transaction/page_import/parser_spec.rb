require 'spec_helper'

describe Transaction::PageImport::Parser do
  subject { described_class }

  context '.call' do
    it 'calls withcolumn' do
      expect_any_instance_of(Transaction::PageImport::Parser::WithColumn).to receive(:call)
      subject.call("", "asdas")
    end

    it 'calls withoutcolumn' do
      expect_any_instance_of(Transaction::PageImport::Parser::WithoutColumn).to receive(:call)
      subject.call("", "")
    end
  end
end
