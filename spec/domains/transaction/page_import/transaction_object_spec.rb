require 'spec_helper'

describe Transaction::PageImport::TransactionObject do
  subject { described_class }

  context '#valid?' do

    it 'returns fail when missing amount' do
      object = subject.new
      expect(object.valid?).to be_falsey
    end

    it 'returns fail when missing date_of_payment' do
      object = subject.new
      expect(object.valid?).to be_falsey
    end

    it 'returns true when have date and amount' do
      object = subject.new
      object.date_of_payment = Date.today
      object.amount = 1
      expect(object.valid?).to be_truthy
    end
  end
end
