require 'spec_helper'

describe Transaction::PageImport::Parser::WithoutColumn do
  subject { described_class }

  context '#call' do
    let(:data) {
      <<-DATA
2015-11-18	2015-11-18	2333434	Sk5568737877	5.157,00	498.095,29
Skicka	2015-11-10	2015-11-09	765434	Pension 	-107,00 	492.938,29
      DATA
    }
    let(:wrong_data) { "" }

    it "returns two transaction objects" do
      transaction_objects = subject.new(data).call
      expect(transaction_objects.length).to eql(2)
    end

    it "return empty" do
      transaction_objects = subject.new(wrong_data).call
      expect(transaction_objects.length).to eql(0)
    end
  end
end
