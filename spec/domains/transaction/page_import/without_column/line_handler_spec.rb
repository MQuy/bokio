require 'spec_helper'

describe Transaction::PageImport::Parser::WithoutColumn::LineHandler do
  subject { described_class }

  context '#convert_to_transaction_object' do
    let(:wrong_line) { 'abasad' }
    let(:correct_line) { 'Skicka	2015-11-10	2015-11-09	765434	Pension 	-107,00 	492.938,29' }
    it 'returns transaction_object' do
      without_column = subject.new(correct_line)
      transaction_object = without_column.convert_to_transaction_object
      expect(transaction_object.valid?).to be_truthy
    end

    it 'returns invalid transaction_object' do
      without_column = subject.new(wrong_line)
      transaction_object = without_column.convert_to_transaction_object
      expect(transaction_object.valid?).to be_falsey
    end
  end
end
