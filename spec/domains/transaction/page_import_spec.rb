require 'rails_helper'

describe Transaction::PageImport do
  subject { described_class }

  context '.call' do
    let(:data) { "Skicka	2015-11-21	2015-11-25	454545	Möbler 	-8.619,60 	489.475,69"  }
    let(:incorrect_data) { "asdasd" }

    it 'gets successful message' do
      response = subject.new(data, "").call
      expect(response.success?).to be_truthy
    end

    it 'gets input data incorrect' do
      response = subject.new("", "").call
      expect(response.success?).to be_falsey
      expect(response.value).to eql("The input data is not correct.")
    end

    it 'get parse wrong message' do
      response = subject.new(incorrect_data, "").call
      expect(response.success?).to be_falsey
      expect(response.value).to eql("We cannot detect your transactions from the data, can you recheck your data?")
    end
  end
end
