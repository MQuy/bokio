module Bokio
  module Response
    def flow(*steps)
      first_result = send(steps[0])
      steps[1..-1].reduce(first_result) do |result, method_name|
        result.bind(method(method_name))
      end
    end

    def response(either)
      if either.success?
        Value.new(true, either.value, :ok)
      else
        error = either.value
        Value.new(false, error.message, error.code)
      end
    end

    def right(object)
      Dry::Monads::Right(object)
    end

    def left(message, code = :bad_request)
      Dry::Monads::Left(Error.new(message, code))
    end

    class Value
      attr_reader :value, :code, :success

      def initialize(success, value, code)
        @value = value
        @code = code
        @success = success
      end

      def success?
        success
      end
    end

    class Error
      attr_reader :message, :code

      def initialize(message, code)
        @message = message
        @code = code
      end
    end
  end
end
