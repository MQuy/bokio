class CreateUserTransaction < ActiveRecord::Migration[5.1]
  def change
    create_table :user_transactions do |t|
      t.date :date_of_payment, null: false
      t.text :description
      t.decimal :amount, precision: 11, scale: 2, null: false
      t.decimal :balance, precision: 11, scale: 2

      t.timestamps
    end
  end
end
