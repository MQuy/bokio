'use strict';

require('../libraries/utils/exts');
require('../libraries/utils/config');
require('../libraries/utils/formManager');
require('../libraries/decorators/index');
require('../libraries/services/index');
require('../libraries/filters/index');
require('../libraries/directives/index');
require('../libraries/resources/index');
require('../libraries/components/index');
require('./homepage/index');
require('./transactions/index');

angular.module('bokio', [
  'decorators',
  'services',
  'filters',
  'directives',
  'resources',
  'components',
  'homepage',
  'transactions'
]);

angular
  .module('bokio')
  .config(appConfig);

appConfig.$inject = ['$routeProvider', '$locationProvider', '$httpProvider', '$sceProvider', '$compileProvider'];
function appConfig($routeProvider, $locationProvider, $httpProvider, $sceProvider, $compileProvider) {
  $sceProvider.enabled(false);
  $compileProvider.debugInfoEnabled(false);
  $locationProvider.html5Mode(true);
  $locationProvider.hashPrefix('#');
  $httpProvider.defaults.headers.common.Accept = 'application/json, text/plain';
  $routeProvider.otherwise({
    redirectTo: '/'
  });
}
