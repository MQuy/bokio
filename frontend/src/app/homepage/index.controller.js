'use strict';

import FormManager from '../../libraries/utils/formManager';

class HomepageCtrl {
  constructor($scope, UserTransaction) {
    this.formManager = new FormManager(this, 'TransactionForm');
    this.UserTransaction = UserTransaction;
  }
  formSubmit() {
    if (this.TransactionForm.$valid) {
      this.UserTransaction.bulkImport(this.transactionLogs);
    } else {
      this.formManager.errorHandler();
    }
  }
  openData() {
    let popupLogs = window.open("", "Transaction Logs", "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=640, height=480, top=100, left=100");
    popupLogs.document.body.innerHTML = `
<!DOCTYPE html>
<html>
  <head>
    <style>
    table {
      font-family: -apple-system, BlinkMacSystemFont, "Open Sans", "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
      border: 1px solid #ddd;
      border-collapse: collapse;
      font-size: 14px;
      width: 100%;
    }

    td, th {
      border: 1px solid #ddd;
      text-align: left;
      padding: 8px;
    }

    tr:nth-child(even) {
      background-color: #f9f9f9;
    }
    </style>
  </head>
  <body>

    <table>
      <thead>
        <tr>
          <th>Date of payment</th>
          <th>Description</th>
          <th>Amount</th>
          <th>Balance</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>2017-07-31</td>
          <td>SKATTEVERKET</td>
          <td>72 157,00</td>
          <td>83 297,75</td>
        </tr>
        <tr>
          <td>2017-06-14</td>
          <td>SpeedLedger</td>
          <td>-12 570,00</td>
          <td>11 140,75</td>
        </tr>
        <tr>
          <td>2017-06-12</td>
          <td>Dustin AB</td>
          <td>-931,00</td>
          <td>23 710,75</td>
        </tr>
        <tr>
          <td>2017-05-20</td>
          <td>00000008889311</td>
          <td>-1 353,00</td>
          <td>24 641,75</td>
        </tr>
        <tr>
          <td>2017-04-10</td>
          <td>BINERO AB</td>
          <td>-563,00</td>
          <td>25 994,75</td>
        </tr>
      </tbody>
    </table>

  </body>
</html>
    `
  }
}

HomepageCtrl.$inject = ['$scope', 'UserTransaction'];

export default HomepageCtrl;
