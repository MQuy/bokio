'use strict';

import HomepageCtrl from './index.controller';

function HomepageConfig($routeProvider) {
  $routeProvider
    .when('/', {
      title: 'Transaction Logs Import',
      templateUrl: 'app/homepage/index.html',
      controller: HomepageCtrl,
      controllerAs: 'vm',
      resolve: HomepageConfig.resolve
    });
}

HomepageConfig.$inject = ['$routeProvider'];

HomepageConfig.resolve = {
};

angular
  .module('homepage', ['ngRoute'])
  .config(HomepageConfig);
