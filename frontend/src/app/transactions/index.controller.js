'use strict';

class TransactionsCtrl {
  constructor($scope, userTransactions) {
    this.userTransactions = userTransactions;
  }
}

TransactionsCtrl.$inject = ['$scope', 'userTransactions'];

export default TransactionsCtrl;
