'use strict';

import TransactionsCtrl from './index.controller';

function TransactionsConfig($routeProvider) {
  $routeProvider
    .when('/transactions', {
      title: 'Transaction Logs',
      templateUrl: 'app/transactions/index.html',
      controller: TransactionsCtrl,
      controllerAs: 'vm',
      resolve: TransactionsConfig.resolve
    });
}

TransactionsConfig.$inject = ['$routeProvider'];

TransactionsConfig.resolve = {
  userTransactions: ['UserTransaction', function(UserTransaction) {
    return UserTransaction.all();
  }]
};

angular
  .module('transactions', ['ngRoute'])
  .config(TransactionsConfig);
