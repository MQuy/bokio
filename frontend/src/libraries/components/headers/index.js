'use strict';

class HeaderCtrl {
  constructor($scope) {
    this.$scope = $scope;
  }
}

HeaderCtrl.$inject = ['$scope'];

angular.module('components.headers', [])
  .controller('HeaderCtrl', HeaderCtrl);

