'use strict';

import Config from '../utils/config';

angular
  .module('decorators.$http', [])
  .config(httpConfig);

httpConfig.$inject = ['$provide'];
function httpConfig($provide) {
  $provide.decorator('$http', httpDecorator);
}

httpDecorator.$inject = ['$injector', '$delegate']
function httpDecorator($injector, $http) {
  $http.getData = httpStub('get');
  $http.postData = httpStub('post');
  $http.putData = httpStub('put');
  $http.destroyData = httpStub('delete');

  return $http;

  function httpStub(method, urlPrefix) {
    return function(url, config) {
      if (method === 'post' || method === 'put') {
        var data = arguments[1];
        config = arguments[2] || {};
        config.data = denormailizeData(data);
      }
      config = config || {};
      config.cache = config.cache || false;
      config.url = Config.apiUrl + (urlPrefix ? '/' + urlPrefix + url : url);
      config.method = method;
      config.params = denormailizeData(config.params);
      config.headers = denormailizeData(_.merge(config.headers || {}));
      return $http(config).then(function(response) {
        return normalizeData(response.data);
      });
    };
  };
}

function normalizeData(data) {
  var results = _.isArray(data) ? [] : {};

  _.each(data, function(value, key) {
    var nv = _.isObject(value) ? normalizeData(value) : value;
    results[_.isNumber(key) ? key : inflection.camelize(key, true)] = nv;
  });

  return results;
}

function denormailizeData(data) {
  var results = _.isArray(data) ? [] : {};

  _.each(data, function(value, key) {
    var dnv = _.isObject(value) ? denormailizeData(value) : value;
    results[underscoreKey(key)] = dnv;
  });

  return results;
}

function underscoreKey(key) {
  var udkey;

  if (_.isString(key)) {
    udkey = inflection.underscore(key);
    if (_.startsWith(key, '_')) udkey = '_' + udkey;
  } else {
    udkey = key;
  }
  return udkey;
}
