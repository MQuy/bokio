angular
  .module('decorators.$rootScope', [])
  .config(rootScopeConfig);

rootScopeConfig.$inject = ['$provide'];
function rootScopeConfig($provide) {
  $provide.decorator('$rootScope', rootScopeDecorator);
}

rootScopeDecorator.$inject = ['$delegate'];
function rootScopeDecorator($rootScope) {
  $rootScope.$safeApply = function(fn) {
    var phase = this.$root.$$phase;
    if(phase == '$apply' || phase == '$digest') {
      if(fn && (typeof(fn) === 'function')) {
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };

  return $rootScope;
}
