'use strict';

function priceFilter() {
  return function(value) {
    return value.toString().replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1\.") + ` ${currency}`;
  };
}

angular
  .module('filters.price', [])
  .filter('price', priceFilter);
