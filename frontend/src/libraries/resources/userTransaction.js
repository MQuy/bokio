'use strict';

function UserTransaction($http) {
  var baseUrl = '/user_transactions';

  return {
    all: function() {
      return $http.getData(`${baseUrl}`);
    },
    bulkImport: function(data) {
      return $http.postData(`${baseUrl}/bulk_import`, { data: data });
    }
  }
}
UserTransaction.$inject = ['$http'];

angular
  .module('resources.userTransaction', [])
  .factory('UserTransaction', UserTransaction);
