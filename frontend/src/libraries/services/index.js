'use strict';

require('./format');
require('./flash');
require('./loadingInterceptor');
require('./httpInterceptor');

angular.module('services', [
  'services.format',
  'services.flash',
  'services.loadingInterceptor',
  'services.httpInterceptor'
]);
