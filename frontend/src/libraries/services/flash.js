'use strict';

function flashConfig() {
  this.$get = flashService;
}

flashRun.$inject = ['$injector'];
function flashRun($injector) {
  $injector.get('$flash');
}

flashService.$inject = ['$rootScope'];
function flashService($rootScope) {
  var exports = {};
  var previousMessage = {};
  var currentMessage = {};
  var standardType = ['warning', 'error', 'success', 'info', 'notice'];
  var defaultType = 'warning';
  var keep = false;

  exports.all = getAllMessages;
  exports.clear = clearMessages;
  exports.keep = keepState;
  exports.set = setMessage;
  exports.get = getMessage;
  exports.now = showNow;

  $rootScope.$on('$routeChangeStart', function(scope, current, pre) {
    _.empty(currentMessage);
    _.defaults(currentMessage, previousMessage);
    if (!keep) _.empty(previousMessage);
    keep = false;
  });

  return exports;

  function getAllMessages() {
    return currentMessage;
  }

  function clearMessages(type) {
    if (typeof type == 'undefined' || type == 'current') _.empty(currentMessage);
    if (typeof type == 'undefined' || type == 'previous') _.empty(previousMessage);
  }

  function keepState() {
    keep = true;
  }

  function setMessage(messages, type, isNow) {
    clearMessages();
    type = normalizeType(type);
    messages = _.isArray(messages) ? messages : [messages];
    var state = (typeof isNow  == 'undefined' || false) ? previousMessage : currentMessage;
    if (typeof state[type] == 'undefined') state[type] = [];
    // using timeout here to overcome dirty check in angularjs
    setTimeout(function() {
      if (state[type]) Array.prototype.push.apply(state[type], messages);
    });
  }

  function getMessage(type) {
    return currentMessage[type];
  }

  function showNow(messages, type) {
    setMessage(messages, type, true);
  }

  function normalizeType(type) {
    return (standardType.indexOf(type) == -1) ? defaultType : type;
  }
}

angular
  .module('services.flash', [])
  .provider('$flash', flashConfig)
  .run(flashRun);
