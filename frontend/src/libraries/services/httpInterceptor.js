'use strict';

httpInterceptorConfig.$inject = ['$httpProvider'];
function httpInterceptorConfig($httpProvider) {
  $httpProvider.interceptors.push('httpInterceptor');
}

httpInterceptorService.$inject = ['$injector', '$q', '$flash', '$location'];
function httpInterceptorService($injector, $q, $flash, $location) {
  var services = {
    response: responseSuccess,
    responseError: responseError
  };

  return services;

  function responseSuccess(response) {
    var dataResponse = response.data;

    if (dataResponse.successes) {
      _.each(dataResponse.successes, function(message) {
        $flash.now(message, 'success');
      })
    } else if (dataResponse.errors) {
      _.each(dataResponse.errors, function(message) {
        $flash.now(message, 'error');
      })
    }

    return response || $q.when(response);
  }

  function responseError(response) {
    return $q.reject(response);
  }
}

angular
  .module('services.httpInterceptor', ['services.flash'])
  .config(httpInterceptorConfig)
  .factory('httpInterceptor', httpInterceptorService);
