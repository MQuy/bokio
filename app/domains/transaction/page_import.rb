module Transaction
  class PageImport
    include Bokio::Response

    def initialize(data, column_names)
      @data = data
      @column_names = column_names
    end

    def call
      result = flow(:check_data, :parse_data, :store_transactions)
      response(result)
    end

    def check_data
      if data.present?
        right(data)
      else
        left("The input data is not correct.")
      end
    end

    def parse_data(data)
      transaction_objects = Parser.call(data, column_names)
      if transaction_objects.present?
        right(transaction_objects)
      else
        left("We cannot detect your transactions from the data, can you recheck your data?")
      end
    end

    def store_transactions(transaction_objects)
      Store.new(transaction_objects).call
      right("There are #{transaction_objects.length} transactions were imported successfully.")
    rescue StandardError => e
      left("There is something wrong when importing your data.")
    end

    private

    attr_reader :data, :column_names
  end
end
