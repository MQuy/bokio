module Transaction
  class PageImport
    class TransactionObject

      attr_accessor :date_of_payment, :description, :amount, :balance

      def valid?
        date_of_payment.present? && amount.present?
      end

      def to_hash
        { date_of_payment: date_of_payment, description: description, amount: amount, balance: balance }
      end
    end
  end
end
