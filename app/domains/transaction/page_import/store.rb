module Transaction
  class PageImport
    class Store

      def initialize(transaction_objects)
        @transaction_objects = transaction_objects
      end

      def call
        user_transactions = []
        UserTransaction.transaction do
          transaction_objects.each do |transaction_object|
            user_transactions << UserTransaction.new(transaction_object.to_hash)
          end
          UserTransaction.import(user_transactions)
        end
      end

      private

      attr_reader :transaction_objects
    end
  end
end
