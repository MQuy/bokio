module Transaction
  class PageImport
    module Parser
      extend self

      def call(data, column_names)
        if column_names.present?
          WithColumn.new(data, column_names).call
        else
          WithoutColumn.new(data).call
        end
      end
    end
  end
end
