module Transaction
  class PageImport
    module Parser
      class WithoutColumn
        LINE_SEPARATOR = /[\n\r]+/

        def initialize(data)
          @data = data
          @lines = data.split(LINE_SEPARATOR).compact.uniq
        end

        def call
          lines.inject([]) do |rows, line|
            transaction = get_transaction(line)
            transaction.valid? ? rows << transaction : rows
          end
        end

        private

        attr_reader :data, :lines

        def get_transaction(line)
          LineHandler.new(line).convert_to_transaction_object
        end
      end
    end
  end
end
