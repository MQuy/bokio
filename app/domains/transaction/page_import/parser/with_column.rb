module Transaction
  class PageImport
    module Parser
      class WithColumn

        def initialize(data, column_names)
          @data = data
          @column_names = column_names
        end

        def call
        end

        private

        attr_reader :data, :column_names
      end
    end
  end
end
