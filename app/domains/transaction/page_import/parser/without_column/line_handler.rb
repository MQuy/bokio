module Transaction
  class PageImport
    module Parser
      class WithoutColumn
        class LineHandler
          COLUMN_SEPARATOR = /[\t\r\n]+|(\s{3,})/

          def initialize(line)
            @line = line
            @columns =
              line
                .split(COLUMN_SEPARATOR)
                .select { |column| column.present? }
          end

          def convert_to_transaction_object
            transaction_object = TransactionObject.new

            columns.each do |column|
              column = column.strip
              if transaction_object.date_of_payment.blank? && date = get_date(column)
                transaction_object.date_of_payment = date
              elsif check_number(column) && number = get_number(column)
                transaction_object.amount = transaction_object.balance
                transaction_object.balance = number
              else
                transaction_object.description = column
              end
            end
            normalize_transaction_object(transaction_object)

            transaction_object
          end

          private

          attr_reader :line, :columns

          def normalize_transaction_object(transaction_object)
            if transaction_object.amount.blank?
              transaction_object.amount == transaction_object.balance
              transaction_object.balance = nil
            end
          end

          def get_date(column)
            Date.parse(column) rescue nil
          end

          def check_number(column)
            column =~ /\A-?[0-9\.\,\s]+\z/ && column =~ /[,\.]/
          end

          def get_number(column)
            string_number = column.gsub(/[\.\,\s]/, "")
            string_number.to_f / 100
          end
        end
      end
    end
  end
end
