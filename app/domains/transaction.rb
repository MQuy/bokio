module Transaction
  extend self

  def import_from_page(data, column_names)
    PageImport.new(data, column_names).call
  end
end
