class UserTransactionsController < ApplicationController

  def index
    user_transactions = UserTransaction.all

    render json: user_transactions, each_serializer: UserTransactionSerializer
  end

  def bulk_import
    response = Transaction.import_from_page(params[:data], params[:column_names])

    if response.success?
      render json: { successes: [response.value] }
    else
      render json: { errors: [response.value] }
    end
  end
end
