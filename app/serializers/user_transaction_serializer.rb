class UserTransactionSerializer < ActiveModel::Serializer
  attributes :id, :date_of_payment, :description, :amount, :balance

  def amount
    sprintf('%.2f', object.amount)
  end

  def balance
    sprintf('%.2f', object.balance)
  end
end
