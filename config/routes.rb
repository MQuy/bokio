Rails.application.routes.draw do

  resources :user_transactions, only: [:index] do
    collection do
      post :bulk_import
    end
  end
end
